#!/bin/bash
set -e

# Remove a potentially pre-existing server.pid for Rails.
rm -f /app/tmp/pids/server.pid

# wait for database
wait-for-it -t 120 database:3306

# Then exec the container's main process (what's set as CMD in the Dockerfile).
exec "$@"