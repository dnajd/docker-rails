.DEFAULT_GOAL := help
BLUE := $(shell tput setaf 4)
RESET := $(shell tput sgr0)
help:
	@printf "\n# Help\n\n"
	@grep -E '^[^ .]+: .*?## .*$$' $(MAKEFILE_LIST) \
		| awk '\
			BEGIN { FS = ": .*##" };\
			{ gsub(";", "\n\t\t      ") }; \
			{ printf "%-20s$(RESET) %s\n", $$1, $$2 }'
	@printf "\n"

# https://www.gnu.org/software/make/manual/make.html#Introduction
MAKE_FILE := $(lastword $(MAKEFILE_LIST))

init: ## SETUP: bootstrap a brand new rails app
	./bin/init

#
# ADDON
#

esbuild_it: ## ADDON: esbuild
	@docker compose run --rm web bin/addon/esbuild_it
	@./bin/restart

sass_it: ## ADDON: enable scss
	@docker compose run --rm web bin/addon/sass_it
	@./bin/restart

heroku_it: ## ADDON: enable heroku
	@docker compose run --rm web bin/addon/heroku_it
	@./bin/restart

graphql_it: ## ADDON: graphql
	@docker compose run --rm web bin/addon/graphql_it
	@./bin/restart

react_it: ## ADDON: react
	@docker compose run --rm web bin/addon/react_it
	@./bin/restart

apollo_client_it: ## ADDON: apollo client;
	@docker compose run --rm web bin/addon/apollo_client_it
	@./bin/restart

#
# WORK
#

revert: ## revert files to last committed state
	@bin/ownit
	git clean -fdx
	git checkout .

reset: ## refresh your docker image, container, gems and database
	./bin/reset

bundle_install: ## install missing/new gems
	docker compose run --rm web bundle install -j10
	docker compose restart web

migrate_db: ## rails db:migrate and db:seed
	bin/rcmd rails db:migrate db:seed

own_it: ## used to set ownership of files generated in dockerfile;
	@bin/ownit

up: ## bring up the app
	docker compose up -d

down: ## bring down the app
	docker compose down

debug: ## live debugging the rails app
	docker compose stop web
	docker compose run --rm --service-ports web

console: ## launch a rails console
	docker compose run --rm web rails console

bash: ## container bash prompt;
	docker compose run --rm web bash

test_all: ## run all tests
	bin/rcmd rails test

test_one: ## run specific tests (TEST=<path> make test_one)
	bin/rcmd rake test TEST=${TEST}

