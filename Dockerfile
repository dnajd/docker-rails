# https://hub.docker.com/_/ruby
FROM ruby:bullseye AS base

# add sources
RUN curl -sL https://deb.nodesource.com/setup_18.x | bash -

RUN curl -sS -o - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" | tee /etc/apt/sources.list.d/google-chrome.list

# get chromedriver
RUN wget https://chromedriver.storage.googleapis.com/2.41/chromedriver_linux64.zip
RUN unzip chromedriver_linux64.zip
RUN mv chromedriver /usr/bin/chromedriver && chown root:root /usr/bin/chromedriver && chmod +x /usr/bin/chromedriver

# install
RUN apt-get update -qq && apt-get install -y \
  build-essential \
  libgconf-2-4 \
  libffi-dev \
  libmariadb-dev-compat \
  libmariadb-dev \
  libssl-dev \
  libxi6 \
  nodejs \
  ruby-dev \
  wait-for-it \
  xvfb \
  zip \
  ;

WORKDIR /app

RUN gem update --system && gem update bundler
RUN npm install -g yarn

COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

# Configure the main process to run when running the image
CMD ["rails", "server", "-b", "0.0.0.0"]

FROM base AS builder

COPY Gemfile Gemfile.lock ./
RUN bundle config set without "development test" && \
  bundle install --jobs=3 --retry=3

FROM base as prod

COPY . /app
COPY --from=builder /usr/local/bundle/ /usr/local/bundle/
