# Rails

Turnkey. Tidy. Rails in docker.

# Get Started

Look at the Makefile to see simple targets to get you started

# Explaination

It would be great if the dev community would make things Tidy. If things reliably stood up with a single command. 

What does all this mean?

You can make a brand new, fully functional, rails application that just works.

```
make init
make up
```

You can take it down.

```
make down
```

You can build your ruby gems any time you want

```
make build
```

You can run rails cli commands and it works.

```
bin/rcmd rails <your_command_here>
```
